# Monkey Butler

Home Assistant + PythonMonkey = Monkey Butler

## The Plan

This is a parallel to the `python_scripts` integration, using the PythonMonkey
module to run JavaScript scripts in a sandbox within Home Assistant.

## Current State

* **Code** 
  * Initial adaptation from `python_scripts` is complete

* **Docker Image** 
  * We have a Docker image with Home Assistant and nodejs/npm
  * HA is currently unable to install pythonmonkey; it appears Alpine Linux is
    unsupported for some dependency

* **Docker-compose**
  * We have a docker-compose file which starts HA successfully with the node+npm
    image


## Next Steps

* Look at pythonmonkey build scripts, see what's invovled in supporting Alpine

